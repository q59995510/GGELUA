安装:
----

打开 ./项目 ,选择自己喜欢的编辑器,把项目文件关联到 ./editor
- Sublime Text 项目文件为*.sublime-project -> ./editor/Sublime Text/sublime_text.exe
- VSCODE 项目文件为*.code-workspace -> ./editor/VSCODE/Code.exe

这里推荐VSCODE

新建项目
-------

复制项目，改名

运行调试
-------

都是F5

由于VSCODE没完善，F5不能定位错误，所以建议使用ctrl+r运行

编译项目
------

- Sublime Text -> 工具-编译
- VSCODE -> 终端-运行生成任务