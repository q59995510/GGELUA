--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-09 04:06:00
--]]

local _ENV = require("GGE界面")
local GGE文本 = require("GGE.文本")

GUI文本 = class("GUI文本",GUI控件,GGE文本)
GUI文本._type = 8
function GUI文本:初始化()
    
    GGE文本.GGE文本(self,self.宽度,self.高度)
end

function GUI文本:_更新(...)
    GUI控件._更新(self,...)
    GGE文本.更新(self,...)
end

function GUI文本:_显示(...)
    GUI控件._显示(self,...)
    local _x,_y = self:取坐标()
    窗口:置区域(_x,_y,self.宽度,self.高度)
        GGE文本.显示(self,_x,_y)
    窗口:置区域()
end

function GUI文本:置高度(v)
    GUI控件.置高度(self,v)
    GGE文本.置高度(self,v)
    return self
end

function GUI文本:置宽度(v)
    GUI控件.置宽度(self,v)
    GGE文本.置宽度(self,v)
    return self
end

function GUI文本:绑定滑块(obj)
    self.滑块 = obj
    if obj then
        obj._置位置 = obj._置位置 or obj.置位置
        obj.置位置 = function (this,v)
            this._置位置(this,v)
            self:置位置(math.floor(this.位置/this.最大值*self._max))
        end
    end
    return obj
end

function GUI文本:_消息事件(msg)
    if not self.是否禁止 then
        for _,v in ipairs(msg.鼠标) do
            if v.type==SDL.鼠标_按下 then
                local cb = self:检查回调(v.x,v.y)
                if cb then
                    v.type = nil
                    if v.button==SDL.BUTTON_LEFT then
                        self._lcb = cb
                        self:发送消息("左键按下",cb,msg)
                    elseif v.button==SDL.BUTTON_RIGHT then
                        self._rcb = cb
                        self:发送消息("右键按下",cb,msg)
                    end
                end
            elseif v.type==SDL.鼠标_弹起 then
                local cb = self:检查回调(v.x,v.y)
                if cb then
                    v.type = nil
                    if v.button==SDL.BUTTON_LEFT  then
                        if cb==self._lcb then
                            self:发送消息("左键弹起",cb,msg)
                        end
                    elseif v.button==SDL.BUTTON_RIGHT then
                        if cb==self._rcb then
                            self:发送消息("右键弹起",cb,msg)
                        end
                    end
                end
                self._lcb = nil
                self._rcb = nil
            elseif v.type==SDL.鼠标_移动 then
                if v.state==0 then
                    local cb = self:检查回调(v.x,v.y)
                    if cb then
                        v.type = nil
                        self._focus = true
                        self:发送消息("获得鼠标",v.x,v.y,cb,msg)
                    elseif self._focus then
                        self._focus = nil
                        self:发送消息("失去鼠标",v.x,v.y,msg)
                    end
                end
            elseif v.type==SDL.鼠标_滚轮 then
                local _,x,y = SDL._wins[v.windowID]:取鼠标状态()
                if self:检查点(x,y) then
                    local p = self:取位置()
                    self:置位置(p+(-v.y*10))
                    if p~=self:取位置() then--滚动成功才吃消息
                        v.type = nil
                        self:发送消息("鼠标滚轮",v.y,msg)
                    end
                    if self.滑块 then
                        self.滑块:_置位置(math.floor(math.abs(self._py)/self._max*self.滑块.最大值))
                    end
                end
            end
        end
    end
end
